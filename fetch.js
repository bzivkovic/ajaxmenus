$('#brand').change(function () {
    $('#item').css({"visibility": "visible"});
    $.getJSON(
        'fetch.php',
        'brand=' + $('#brand').val(),
        function (result) {
            $('#item').empty();
            $('#battery').empty();
            $('#battery').css({"visibility": "hidden"});
            var bojan = 0;
            $.each(result.result, function () {
                bojan++;
                $('#item').append('<option>' + this['item'] + '</option>');
            });
            if (bojan < 2) {
                $('#battery').css({"visibility": "visible"});
                console.log($('#item').val());
                batteryValue();
            }
        }
    );
});

$('#item').change(function () {
    // alert('fetch2.php' + 'item='+$('#item').val());
    $('#battery').css({"visibility": "visible"});
    batteryValue();
});

function batteryValue() {
    $.getJSON(
        'fetch2.php',
        'item=' + $('#item').val(),
        function (result) {
            $('#battery').empty();
            $.each(result.result, function () {
                // alert(this['battery']);
                $('#battery').append('<option>' + this['battery'] + '</option>');
            });
        }
    );
}
